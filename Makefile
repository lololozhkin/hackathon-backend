all: build down migrate collectstatic up

pull:
	docker-compose pull

push:
	docker-compose push

build:
	docker-compose build

up:
	docker-compose up -d

down:
	docker-compose down

# $m [migration]
migrate:
	docker-compose run app sh -c '/wait && python manage.py migrate $(if $m,app $m,)'

createsuperuser:
	docker-compose run app python manage.py createsuperuser

collectstatic:
	docker-compose run app python manage.py collectstatic --no-input --clear

makemigrations:
	docker-compose run --rm --volume=${PWD}/src:/src app python manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

dev:
	docker-compose run --volume=${PWD}/src:/src --publish=8000:8000 app /wait && uvicorn --workers 3 --port 8000 config.asgi:application --reload

sh:
	docker exec -it hackathon-backend__$c sh

psql:
	docker exec -it hackathon-backend__db psql -U postgres

shell:
	docker-compose run app python manage.py shell

# $f [filename]
dotenv:
	docker build -t commands ./commands
	docker run commands /bin/sh -c 'python generate_dotenv.py && cat generate_dotenv/.env.example' > $(if $f,$f,.env.tmp)

piplock:
	docker-compose run --rm --no-deps --volume=${PWD}/src:/src --workdir=/src app pipenv install
	sudo chown -R ${USER} src/Pipfile.lock

import-sites:
	docker-compose run --rm --volume=${PWD}/src:/src app bash -c '/wait && python manage.py import_sites'


.PHONY: all pull push build up down migrate createsuperuser collectstatic makemigrations dev sh psql shell dotenv piplock
