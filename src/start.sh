#!/bin/bash

/wait && uvicorn --workers 3 --host 0.0.0.0 --port 8000 config.asgi:application
