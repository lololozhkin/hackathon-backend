from typing import List

from ninja import Schema


class DomainDTO(Schema):
    domain: str

    class Config:
        title = 'DomainData'


class GetAllDomainsIn(Schema):
    class Config:
        title = 'GetAllDomainsRequest'


class GetAllDomainsOut(Schema):
    domains: List[DomainDTO]

    class Config:
        title = 'GetAllDomainsResponse'