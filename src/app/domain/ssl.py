from datetime import date
from typing import List

from ninja import Schema


class SSLCheckIn(Schema):
    domain: str


class SSLCheckOut(Schema):
    host_name:   str
    peer_ip:     str
    common_name: str
    SAN:         List[str]
    issuer:      str
    crt_date:    date
    exp_date:    date
