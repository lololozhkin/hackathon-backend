from typing import Optional, List

from ninja import Schema


class CheckOut(Schema):
    safety: bool
    phishing_probability: Optional[float]
    reasons: Optional[List[str]]


class CheckIn(Schema):
    domain: str


class CheckResponse(Schema):
    verdict: str
    probability: float
    results: List[CheckOut]
