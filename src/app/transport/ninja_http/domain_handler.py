from typing import List

from app.controllers.domain_controller import DomainController
from app.domain.domains import GetAllDomainsIn, DomainDTO


class DomainHandler:
    def __init__(self, domain_controller: DomainController):
        self._domain_controller = domain_controller

    async def get_all(self, request) -> List[DomainDTO]:
        return await self._domain_controller.get_all()

    async def check_ssl(self, request, domain: DomainDTO):
        return await self._domain_controller.check_ssl(domain.domain, 443)
