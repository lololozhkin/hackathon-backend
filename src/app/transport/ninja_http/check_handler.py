import random

from app.controllers.checker import CheckerController
from app.domain.checks import CheckIn, CheckResponse


class CheckHandler:
    def __init__(self, check_controller: CheckerController):
        self._check_controller = check_controller

    async def check(self, request, domain_data: CheckIn):
        results = await self._check_controller.check_domain(domain_data)
        total = 0
        not_none_probs = 0
        successes = 0

        for res in results:
            if res.phishing_probability is not None:
                total += res.phishing_probability
                not_none_probs += 1

            successes += int(res.safety)

        succ_rate = successes / len(results)
        safety = succ_rate >= 0.5
        if safety:
            verdict = 'We think, that this site is not safety'
        else:
            verdict = 'We think, that this site is safety'

        return CheckResponse(
            verdict=verdict,
            probability=total / not_none_probs,
            results=results,
        )
