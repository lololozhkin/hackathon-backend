from typing import List

from asgiref.sync import sync_to_async

from app.domain.domains import DomainDTO
from app.infrastructure.models.domain import DomainModel


class DomainRepo:
    @sync_to_async
    def get_all(self):
        return [
            DomainDTO.from_orm(model)
            for model in DomainModel.objects.all()
        ]
