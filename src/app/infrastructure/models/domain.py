from django.db import models


class DomainModel(models.Model):
    domain = models.CharField(max_length=255, primary_key=True)
