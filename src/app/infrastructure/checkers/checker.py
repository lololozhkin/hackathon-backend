from abc import ABC, abstractmethod

from app.domain.checks import CheckOut


class Checker(ABC):
    @abstractmethod
    async def check(self, domain: str) -> CheckOut:
        pass
