from app.domain.checks import CheckOut
from app.infrastructure.checkers.checker import Checker


BANNED_SYMBOLS = ['@', '!', '^', '*']


class HeuristicsCheck(Checker):
    async def check(self, domain: str) -> CheckOut:
        safety = True
        reasons = []
        prob = 0
        if len(domain) > 30:
            safety = False
            reasons.append('Domain is too long, please be careful')
            prob += 0.3

        if len(domain.split('.')) > 4:
            safety = False
            reasons.append('Domain is too nested, it may be dangerous')
            prob += 0.3

        if any([symbol in domain for symbol in BANNED_SYMBOLS]):
            safety = False
            reasons.append('Some strange symbols are in domain, check them out')
            prob += 0.3

        return CheckOut(
            safety=safety,
            phishing_probability=prob,
            reasons=reasons,
        )

