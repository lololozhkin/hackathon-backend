from asgiref.sync import sync_to_async
from django.db.models import Func
from django.db.models.expressions import F

from app.domain.checks import CheckOut
from app.infrastructure.checkers.checker import Checker
from app.infrastructure.models.domain import DomainModel


class Levenshtein(Func):
    template = "%(function)s(%(expressions)s, '%(search_term)s')"
    function = "levenshtein"

    def __init__(self, expression, search_term, **extras):
        super(Levenshtein, self).__init__(
            expression,
            search_term=search_term,
            **extras
        )


class LevenshteinChecker(Checker):
    @sync_to_async
    def find_closest(self, pattern):
        close_objects = DomainModel.objects.annotate(
            lev_dist=Levenshtein(F('domain'), pattern)
        ).filter(
            lev_dist__lte=3,
        )
        containing_objects = DomainModel.objects.filter(
            domain__contains=pattern,
        )

        return [
            *[_ for _ in close_objects],
            # *[_ for _ in containing_objects]
        ]

    async def check(self, domain: str) -> CheckOut:
        parts = domain.split('.')
        parts.append(domain)
        if len(parts) > 6:
            return CheckOut(safety=False, reasons=['Too nested domain'])

        min_lev_dist = 10**9
        min_lev_dist_domain = ''
        for part in parts:
            closest = await self.find_closest(part)
            for row in closest:
                if row.domain == domain:
                    return CheckOut(
                        safety=True,
                        phishing_probability=0,
                        reasons=['Site was found in base of good sites']
                    )
                if hasattr(row, 'lev_dist'):
                    if row.lev_dist < min_lev_dist:
                        min_lev_dist_domain = row.domain
                        min_lev_dist = min_lev_dist
                        continue

                return CheckOut(
                    safety=True,
                    phishing_probability=1,
                    reasons=[f'Site is similar to {row.domain}, be careful']
                )

        if min_lev_dist == 10**9:
            return CheckOut(
                safety=True,
                phishing_probability=0,
                reasons=['Similar sites has not been found in our database'],
            )

        return CheckOut(
            safety=True,
            phishing_probability=1,
            reasons=[f'Site is similar to {min_lev_dist_domain}, be careful']
        )
