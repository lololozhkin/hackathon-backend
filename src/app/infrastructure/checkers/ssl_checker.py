from datetime import datetime, timedelta

from OpenSSL import SSL
from cryptography import x509
from cryptography.x509.oid import NameOID
import idna

from socket import socket
from collections import namedtuple

from app.domain.checks import CheckOut
from app.domain.ssl import SSLCheckOut
from app.infrastructure.checkers.checker import Checker

HostInfo = namedtuple(field_names='cert hostname peer_ip', typename='HostInfo')

HOSTS = [
    ('damjan.softver.org.mk', 443),
    ('expired.badssl.com',    443),
    ('wrong.host.badssl.com', 443),
    ('ca.ocsr.nl',            443),
    ('vk.com',                443),
    ('самодеј.мкд',           443),
]


def verify_cert(cert, hostname):
    # verify notAfter/notBefore, CA trusted, servername/sni/hostname
    cert.has_expired()
    # service_identity.pyopenssl.verify_hostname(client_ssl, hostname)
    # issuer


def get_certificate(hostname, port):
    hostname_idna = idna.encode(hostname)
    sock = socket()

    sock.connect((hostname, port))
    peer_ip = sock.getpeername()[0]
    ctx = SSL.Context(SSL.SSLv23_METHOD) # most compatible
    ctx.check_hostname = False
    ctx.verify_mode = SSL.VERIFY_NONE

    sock_ssl = SSL.Connection(ctx, sock)
    sock_ssl.set_connect_state()
    sock_ssl.set_tlsext_host_name(hostname_idna)
    sock_ssl.do_handshake()
    cert = sock_ssl.get_peer_certificate()
    crypto_cert = cert.to_cryptography()
    sock_ssl.close()
    sock.close()

    return HostInfo(cert=crypto_cert, peer_ip=peer_ip, hostname=hostname)


def get_alt_names(cert):
    try:
        ext = cert.extensions.get_extension_for_class(x509.SubjectAlternativeName)
        return ext.value.get_values_for_type(x509.DNSName)
    except x509.ExtensionNotFound:
        return None


def get_common_name(cert):
    try:
        names = cert.subject.get_attributes_for_oid(NameOID.COMMON_NAME)
        return names[0].value
    except x509.ExtensionNotFound:
        return None


def get_issuer(cert):
    try:
        names = cert.issuer.get_attributes_for_oid(NameOID.COMMON_NAME)
        return names[0].value
    except x509.ExtensionNotFound:
        return None


def result_info(hostinfo):
    res = {
        'host_name':   hostinfo.hostname,
        'peer_ip':     hostinfo.peer_ip,
        'common_name': get_common_name(hostinfo.cert),
        'SAN':         get_alt_names(hostinfo.cert),
        'issuer':      get_issuer(hostinfo.cert),
        'crt_date':    hostinfo.cert.not_valid_before,
        'exp_date':    hostinfo.cert.not_valid_after,
    }

    return SSLCheckOut(**res)


def check_it_out(hostname, port):
    hostinfo = get_certificate(hostname, port)
    print(result_info(hostinfo))


class SSLChecker(Checker):
    async def check(self, domain: str) -> CheckOut:
        try:
            data = self.check_ssl(domain)
        except Exception:
            return CheckOut(
                safety=False,
                reasons=['Please, dont use servers, working on http'],
                phishing_probability=1
            )

        safety = True
        reasons = []
        if data.exp_date < datetime.now().date():
            safety = False
            reasons.append('SSL certificate has been expired')

        if datetime.now().date() - data.crt_date < timedelta(days=14):
            safety = False
            reasons.append('SSL certificate is too young, registered less than 2 weeks ago')

        prob = len(reasons) * 2 / 3
        if prob > 0:
            return CheckOut(safety=safety, phishing_probability=prob, reasons=reasons)

        return CheckOut(safety=safety)

    def check_ssl(self, host, port=443) -> SSLCheckOut:
        return result_info(get_certificate(host, port))


if __name__ == '__main__':
    import concurrent.futures

    with concurrent.futures.ThreadPoolExecutor(max_workers=4) as e:
        for hostinfo in e.map(lambda x: get_certificate(x[0], x[1]), HOSTS):
            print(result_info(hostinfo))

