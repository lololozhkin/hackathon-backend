import os

from django.core.management.base import BaseCommand

from app.infrastructure.models.domain import DomainModel


class Command(BaseCommand):
    def handle(self, *args, **options):
        file_path = os.path.join(os.getcwd(), 'app/management/commands/alexa-top-20000-sites.txt')
        with open(file_path, 'r') as f:
            for index, line in enumerate(f):
                DomainModel.objects.update_or_create(domain=line.strip())
                if index % 500 == 0:
                    print(index)

        print('Done!')
