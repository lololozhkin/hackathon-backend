from typing import List

from app.domain.checks import CheckIn, CheckOut
from app.infrastructure.checkers.checker import Checker


class CheckerController:
    def __init__(self, checkers: List[Checker]):
        self._checkers = checkers

    async def check_domain(self, domain_data: CheckIn) -> List[CheckOut]:
        domain = domain_data.domain
        results = [
            await checker.check(domain)
            for checker in self._checkers
        ]

        return results
