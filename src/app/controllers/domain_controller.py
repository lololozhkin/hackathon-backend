from typing import List

from app.domain.domains import DomainDTO
from app.infrastructure.checkers.ssl_checker import SSLChecker
from app.infrastructure.repositories.domain_repo import DomainRepo


class DomainController:
    def __init__(self, domain_repo: DomainRepo, ssl_checker: SSLChecker):
        self._domain_repo = domain_repo
        self._ssl_checker = ssl_checker

    async def get_all(self) -> List[DomainDTO]:
        return await self._domain_repo.get_all()

    async def check_ssl(self, host, port=443):
        return self._ssl_checker.check_ssl(host, port)
