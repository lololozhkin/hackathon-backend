from typing import List

from django.contrib import admin
from django.urls import path
from ninja import NinjaAPI, Router

from app.controllers.checker import CheckerController
from app.controllers.domain_controller import DomainController
from app.domain.checks import CheckResponse
from app.domain.domains import DomainDTO
from app.domain.ssl import SSLCheckOut
from app.infrastructure.checkers.heuristics_checker import HeuristicsCheck
from app.infrastructure.checkers.levenshtein_checker import LevenshteinChecker
from app.infrastructure.checkers.ssl_checker import SSLChecker
from app.infrastructure.repositories.domain_repo import DomainRepo
from app.transport.ninja_http.check_handler import CheckHandler
from app.transport.ninja_http.domain_handler import DomainHandler


def get_ninja_app():
    router = Router()
    domain_repo = DomainRepo()
    ssl_checker = SSLChecker()
    heuristic_checker = HeuristicsCheck()
    levenshtein_checker = LevenshteinChecker()

    domain_controller = DomainController(domain_repo, ssl_checker)
    check_controller = CheckerController([
        ssl_checker,
        heuristic_checker,
        levenshtein_checker,
    ])

    domain_handler = DomainHandler(domain_controller)
    check_handler = CheckHandler(check_controller)

    router.add_api_operation(
        '/get_all',
        ['GET'],
        domain_handler.get_all,
        response=List[DomainDTO]
    )
    router.add_api_operation(
        '/check_ssl',
        ['POST'],
        domain_handler.check_ssl,
        response=SSLCheckOut,
    )
    router.add_api_operation(
        '/check_domain',
        ['POST'],
        check_handler.check,
        response=CheckResponse,
    )

    ninja_app = NinjaAPI()
    ninja_app.add_router('/domains/', router)

    return ninja_app


app = get_ninja_app()

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', app.urls),
]
